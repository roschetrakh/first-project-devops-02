pipeline {
    agent any
    environment { 
        GIT_REPO = 'https://gitlab.com/roschetrakh/first-project-devops-02.git'
        DOCKER_HUB_PASSWORD = credentials('docker_hub')
    }
    parameters {
        string(name: 'ReleaseNote', defaultValue: 'Initial release', description: 'Enter some information about the release')
    }
    stages {
        stage('Preparation') {
            steps {
                // Clean the workspace at the beginning of the build
                deleteDir()
            }
        }
        stage('Checkout') {
            steps {
                // Checkout the code from the Git repository
                git url: GIT_REPO
            }
        }
        stage('Build') {
            steps {
                // Change to the directory containing the Dockerfile and build/push the Docker image
                script {
                    dir('mtml') { // Ensure 'mtml' is the correct directory; this might be a typo
                        sh '''
                            cd html
                            docker build . -t roschetra/html-php:${BUILD_NUMBER}
                            echo $DOCKER_HUB_PASSWORD | docker login -u roschetra --password-stdin
                            docker push roschetra/html-php:${BUILD_NUMBER}
                        '''
                    }
                }
            }
        }
        stage('Post Build') {
            steps {
                // You can add steps to clean up or perform post-build actions
                echo "Build and push completed successfully."
            }
        }
    }
    post {
        always {
            echo 'This will always run regardless of build result.'
        }
        success {
            echo 'Build was a success!'
        }
        failure {
            echo 'Build failed.'
        }
    }
}
